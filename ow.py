import sqlite3
from flask import Flask, redirect, url_for, request, render_template, send_from_directory
import json
from datetime import datetime as dt

app = Flask(__name__)

db = sqlite3.connect('orders.sqlite', check_same_thread=False)

def insert_ware(ware):
    c = db.cursor()
    q = 'INSERT INTO ware (guid,name) VALUES(?,?)'
    c.execute(q,(ware['GUID'], ware['Name']))

def insert_client(cl):
    c = db.cursor()
    q = 'INSERT INTO clients (guid,name,taxid) VALUES(?,?,?)'
    c.execute(q,(cl['GUID'], cl['Name'], cl['TaxID']))

def insert_contract(con):
    c = db.cursor()
    q = 'INSERT INTO contracts (guid,name,owner,currency) VALUES(?,?,?,?)'
    c.execute(q,(con['GUID'], con['Name'], con['Owner'], con['Currency'],))

def last_version(guid, c = None):
    if c == None:
        c = db.cursor()
    res = c.execute('SELECT MAX(version) AS v FROM order_versions WHERE guid=?', (guid,))
    for r in res:
        if not r[0] == None:
            return r[0]
    return None

def get_order(guid, v, c = None):
    if c == None:
        c = db.cursor()
    q1 = '''
SELECT OV.code, OV.date, OV.version, OV.author, OV.committime, OV.sum, OV.currency, CL.name, CN.name
FROM order_versions AS OV
 LEFT JOIN clients AS CL ON CL.guid = OV.clientid
  LEFT JOIN contracts AS CN ON CN.guid = OV.contractid
WHERE ABS (OV.version - ?) <= 1 AND OV.guid=?
ORDER BY OV.version
'''
    q2 = '''SELECT WO.linenum, W.name, WO.qty, WO.price, WO.sum, WO.version
FROM ware_in_order AS WO
  LEFT JOIN ware AS W ON WO.wareid = W.guid
WHERE ABS (WO.version - ?) <= 1 AND WO.orderid=?
ORDER BY WO.version, WO.linenum
'''
    res = c.execute(q1, (v, guid,))
    versions = {'prev': None, 'curr': None, 'next': None}
    for r in res:
        if not r[2] == None:
            o = {
                'Code':     r[0],
                'Date':     r[1],
                'Version':  r[2],
                'Author':   r[3],
                'Time':     r[4],
                'Sum':      r[5],
                'Currency': r[6],
                'Client':   r[7],
                'Contract': r[8],
                'Ware':     []
            }
            if r[2] < v:
                versions['prev'] = o
            elif r[2] == v:
                versions['curr'] = o
            else:
                versions['next'] = o
    res = c.execute(q2, (v, guid,))
    for r in res:
        if not r[5] == None:
            w = {
                'Line':    r[0],
                'Ware':    r[1],
                'Qty':     r[2],
                'Price':   r[3],
                'Sum':     r[4]
            }
            if r[5] < v:
                versions['prev']['Ware'].append(w)
            if r[5] == v:
                versions['curr']['Ware'].append(w)
            if r[5] > v:
                versions['next']['Ware'].append(w)
    m = 0
    for a, ver in versions.items():
        if ver == None:
            continue
        if len(ver['Ware']) > m:
            m = len(ver['Ware'])
    for a, ver in versions.items():
        if ver == None:
            continue
        while len(ver['Ware']) < m:
            ver['Ware'].append({
                             'Line':    '--',
                             'Ware':    '--',
                             'Qty':     '--',
                             'Price':   '--',
                             'Sum':     '--'
                             })
            print(ver['Ware'])
    return versions

def insert_order_version(o):
    c = db.cursor()
    v = last_version(o['GUID'])
    if v == None:
        v = 0
    else:
        v = v + 1
    cd = dt.now().strftime('%d.%m.%Y %H:%M:%S')
    od = dt.strptime(o['Date'],'%Y-%m-%dT%H:%M:%S').strftime('%d.%m.%Y %H:%M:%S')
    q = 'INSERT INTO order_versions (guid,author,committime,version,code,date,clientid,contractid,currency,sum) VALUES(?,?,?,?,?,?,?,?,?,?)'
    c.execute(q, (o['GUID'], o['Author'], cd, v, o['Code'], od, o['Client']['GUID'], o['Contract']['GUID'], o['Currency'], o['Sum'],))
    return v

def insert_ware_line(l, o, v):
    c = db.cursor()
    insert_ware(l['Ware'])
    q = 'INSERT INTO ware_in_order (orderid, version, linenum, wareid, qty, price, sum) VALUES (?,?,?,?,?,?,?)'
    c.execute(q, (o, v, l['LineNum'], l['Ware']['GUID'], l['Qty'], l['Price'], l['Sum'],))

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/setup')
def setup():
    c = db.cursor()
    c.execute('''
CREATE TABLE IF NOT EXISTS order_versions(
 guid text,
 author text,
 committime text,
 version integer,
 code text,
 date text,
 clientid text,
 contractid text,
 currency text,
 sum real,
 PRIMARY KEY (guid, version) ON CONFLICT REPLACE);''')
    c.execute('''
CREATE TABLE IF NOT EXISTS ware(
 guid text,
 name text,
 PRIMARY KEY (guid) ON CONFLICT REPLACE);''')
    c.execute('''
CREATE TABLE IF NOT EXISTS clients(
 guid text,
 name text,
 taxid text,
 PRIMARY KEY (guid) ON CONFLICT REPLACE);''')
    c.execute('''
CREATE TABLE IF NOT EXISTS contracts(
 guid text,
 name text,
 owner text,
 currency text,
 PRIMARY KEY (guid) ON CONFLICT REPLACE);''')
    c.execute('''
CREATE TABLE IF NOT EXISTS ware_in_order(
 orderid text,
 version num,
 linenum int,
 wareid text,
 qty real,
 price real,
 sum real,
 PRIMARY KEY (orderid, version, linenum) ON CONFLICT REPLACE);''')
    return 'OK' #redirect(url_for('get_orders'))

@app.route('/orders')
def get_orders():
    c = db.cursor()
    res = c.execute('''SELECT OV.guid, OV.code, OV.date, OV.version, OV.author, OV.committime, OV.sum, OV.currency FROM 
(SELECT guid, MAX(version) as mv from order_versions GROUP BY guid) AS OM
  LEFT JOIN order_versions as OV
    ON OM.guid = OV.guid
      AND OM.mv = OV.version''')
    orders_list = []
    for r in res:
        o = {}
        o['GUID']     = r[0]
        o['Code']     = r[1]
        o['Date']     = r[2]
        o['Version']  = r[3]
        o['Author']   = r[4]
        o['Time']     = r[5]
        o['Sum']      = r[6]
        o['Currency'] = r[7]
        print(o)
        orders_list.append(o)
    return render_template('orders.html', orders=orders_list)

@app.route('/commit', methods=['POST'])
def commit():
    doc = json.loads(request.data.decode('utf-8-sig'))
    insert_client(doc['Client'])
    insert_contract(doc['Contract'])
    version = insert_order_version(doc)
    for w in doc['Ware']:
        insert_ware_line(w, doc['GUID'], version)
    
#    c = db.cursor()
#    for r in c.execute('select * from ware_in_order'):
#        print(r)
    db.commit()
    return 'OK'

@app.route('/order/<guid>')
def show_order(guid):
    v = request.args.get('v')
    if v != None:
        v = int(v)
    c = db.cursor()
    lv = last_version(guid, c)
    if lv == None:
        return redirect(url_for('get_orders')), 404
    if v == None:
        v = lv 
    versions = get_order(guid, v, c)
    return render_template('order.html', prev = versions['prev'], curr = versions['curr'], nxt = versions['next'], GUID=guid)

@app.route('/css/<path:filename>')
def download_file(filename):
    return send_from_directory('css', filename, as_attachment=False)

if __name__ == '__main__':
    setup()
    app.run(host='0.0.0.0', port=80)
    db.close()
